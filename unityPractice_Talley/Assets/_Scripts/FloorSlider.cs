﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSlider : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.position = transform.position + (new Vector3(10.5f, 0f, 0f) * Time.deltaTime * speed);

        if (transform.position.x >= 13.5)
        {

            transform.position = transform.position + (new Vector3(-10.5f, 0f, 0f) * Time.deltaTime * speed);
        }
        if (transform.position.x <= -13.5)
        {

            transform.position = transform.position + (new Vector3(10.5f, 0f, 0f) * Time.deltaTime * speed);
        }
        
    }
}
